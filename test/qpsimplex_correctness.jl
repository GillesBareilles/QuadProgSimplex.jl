using Test
using Random
using LinearAlgebra
using SparseArrays

@testset "QPsimplex - correctness" begin
    @testset "Float64" begin
        Tf = Float64
        P = Tf[
            0 3 -2
            2 0 1
        ]
        a = zeros(3)

        w_Kiwiel = qpsimplex(P, a)
        w_OSQP = find_minimumnormelt_OSQP(P, a)
        @test w_Kiwiel ≈ w_OSQP
        @test prevfloat(norm(P * w_Kiwiel)) ≤ nextfloat(norm(P * w_OSQP))
        @test QPS.checkoptimality(P, a, w_Kiwiel) <= 10*eps(Tf)

        P = rand(10, 20)
        a = rand(20)
        w_Kiwiel = qpsimplex(P, a)
        w_OSQP = find_minimumnormelt_OSQP(P, a)
        @test w_Kiwiel ≈ w_OSQP
        @test prevfloat(objval(P, a, w_Kiwiel)) ≤ nextfloat(objval(P, a, w_OSQP))
        @test QPS.checkoptimality(P, a, w_Kiwiel) <= 10*eps(Tf)
    end

    @testset "gradient sampling type problem" begin
        # A case close to gradient sampling:
        @testset "pb $npb" for npb in 1:10
            Random.seed!(1643 + npb)
            P = QPS.getbundlelikeproblem(n=20, ngroups=4, nvecpergroup=6)
            a = zeros(6*4)
            w_Kiwiel = qpsimplex(P, a)
            w_OSQP = find_minimumnormelt_OSQP(P, a)
            @test norm(P * w_Kiwiel) ≤ nextfloat(norm(P * w_OSQP))
            @test QPS.checkoptimality(P, a, w_Kiwiel) <= 100*eps(Float64)
        end
    end

    @testset "BigFloat" begin
        P = BigFloat[
            0 3 -2
            2 0 1
        ]
        a = zeros(BigFloat, 3)

        w_Kiwiel = qpsimplex(P, a)
        @test isa(w_Kiwiel, Vector{BigFloat})
        @test w_Kiwiel ≈ BigFloat[0.0, 11//26, 15//26]
    end

    # @testset "Rational" begin
    #     P = Rational[
    #         0 3 -2
    #         2 0 1
    #     ]
    #     a = zeros(Rational, 3)

    #     w_Kiwiel = qpsimplex(P, a)
    #     @test isa(w_Kiwiel, Vector{Rational})
    #     @test w_Kiwiel == [0//1, 11//26, 15//26]
    # end

    @testset "Kiwiel's instances" begin
        @testset  "Tf = $Tf" for Tf in [Float64, BigFloat]
            @testset "j = $jₐ" for jₐ in 1:30
                P, a, x̄, Ĵ = QPS.getKiwieltestpb(;jₐ, n = 15, Tf)

                w_Kiwiel = qpsimplex(P, a)
                res = QPS.checkoptimality(P, a, w_Kiwiel)
                @test res <= 3e1*eps(Tf)
            end
        end
    end
end
