using Test
using Random
using LinearAlgebra
using SparseArrays
using JET
using AllocCheck

@testset "Typestability" begin
    @testset "non-allocating" begin
        @test isempty(AllocCheck.check_allocs(set_matrix!, (QPDFState{Float64}, Matrix{Float64}, )))
        @test isempty(AllocCheck.check_allocs(set_vector!, (QPDFState{Float64}, Vector{Float64}, )))
        @test isempty(AllocCheck.check_allocs(q1edf1!, (QPDFState{Float64}, )))
    end

    @testset "JET" begin
        m = 60
        n = 100
        fais = rand(n, m)

        P = fais' * fais
        b = rand(m)

        state = init(m, m)

        @test_call init(m, m)
        @test_opt init(m, m)

        @test_call q1edf1!(state)
        @test_opt q1edf1!(state)

        @test_call set_matrix!(state, P)
        @test_opt set_matrix!(state, P)

        @test_call set_vector!(state, b)
        @test_opt set_vector!(state, b)
    end

    n = 100
    m = 60
    state = init(m, m)
    @testset "Correctness $i" for i in 1:10
        Random.seed!(1346 + i)
        fais = rand(n, m)
        P = fais' * fais
        b = rand(m)

        # OSQP solution
        res = find_minimumnormelt_OSQP(fais, b)

        # Kiwiel's solution
        set_matrix!(state, P)
        set_vector!(state, b)
        q1edf1!(state)
        @test state.inform == 0
        @test norm(get_minimizer(state) - res) < 1e-14
    end
end
