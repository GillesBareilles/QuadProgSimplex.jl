using OSQP
using SparseArrays
using LinearAlgebra

"""
    find_minimumnormelt_OSQP(P, b)

Solve minimize the quadratic `0.5*xᵀPᵀPx + bᵀx` such that `x` lives in the
simplex set.
"""
function find_minimumnormelt_OSQP(P, b)
    n, nsamples = size(P)

    P = sparse(P' * P)
    A = sparse(vcat(Diagonal(1.0I, nsamples), ones(nsamples)'))
    l = zeros(nsamples + 1)
    l[end] = 1
    u = Inf * ones(nsamples + 1)
    u[end] = 1

    # Solve problem
    options = Dict(
        :verbose => false,
        :polish => true,
        :eps_abs => 1e-06,
        :eps_rel => 1e-06,
        :max_iter => 5000,
    )
    model = OSQP.Model()
    OSQP.setup!(model; P = P, q = b, A = A, l = l, u = u, options...)
    results = OSQP.solve!(model)
    return results.x
end

function find_minimumnormelt_OSQP(P)
    return find_minimumnormelt_OSQP(P, zeros(size(P, 2)))
end

objval(P, a, w) = 0.5*norm(P * w)^2 + dot(a, w)
