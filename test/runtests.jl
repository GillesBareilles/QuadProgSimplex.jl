using QuadProgSimplex
using Test
using OSQP

include("OSQP.jl")

@testset "QuadProgSimplex.jl" begin
    include("nearestpointpolytope.jl")
    include("qpsimplex_correctness.jl")
    include("qpsimplex_typestab.jl")
end
