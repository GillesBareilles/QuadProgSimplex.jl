function q1edf1!(
    state::QPDFState{Tf}
    ) where {Tf}
    #  integer            itermx, msglvl, m, kmax, leng,
    # &                   idelmx, mode, k, l, iter, inform,
    # &                   lenr, idelet, naug, nexc, ndel
    #  integer            js(kmax)
    #  double precision   u, epstop, epscon, ahat, v, vhat, vtilde,
    # &                   w, vgxal, errkt, errx
    #  double precision   g(leng), a(m), x(m),
    # &                   y(kmax), ya(kmax), ye(kmax), yhat(kmax),
    # &                   ytilde(kmax), r(lenr)
    #
    #     Copyright (c) 1996 by Krzysztof C. Kiwiel
    #
    #*****Subprograms called:
    #     FORTRAN-supplied - abs, max, min, sqrt,
    #     q1edf-supplied   - q1edf2.
    #
    #*****History:
    #     Written by Krzysztof C. Kiwiel.
    #     Date last modified: February 18, 1996.
    #
    #*****Body of function q1edf1:
    #     Where possible, names of variables agree with those used in the
    #     description of the method (see the reference in q1edf).

    @unpack_QPDFState state

    i = ia = ig = incrw = incrwm = isetya = isetye = istep = iwold = j = j1 = ja = jadd = jdel = jg = jr = jrot = jrotat = ka = kg = kmin1 = kr = krefac = lenrk = lg = nincrw = nref = 0
    dalpha = delta = deltap = deltaw = dgamma = dmu = dnu = dsigma = dtau = eps1 = eps2 = errorv = half = r1 = r2 = rhosq = s = s1 = t = ustore = violat = wmin = wold = yanew = yaye = ybarl = yenew = yesqr = Tf(0)

    eps1 = Tf(0.5)
    eps2 = Tf(0.01)
    # Initialize counters and indicators for objective tests
    nincrw=3
    incrw=-1
    incrwm=-1
    iwold=0
    # Initialize counters
    naug=0
    nexc=0
    ndel=0
    nref=0
    iter=-1
    ( mode == 3 || mode == 4 || mode == 8 ) && (iter=0)
    jdel=0
    jadd=0
    t=Tf(0)
    ( mode > 1 ) && (lenrk=Int64(k*(k+1)/2))
    # isetya = 1 means the vector ya must be recomputed.
    isetya=0
    ( mode == 2 || mode == 5 || mode == 7) && (isetya=1)
    # isetye = 1 means the vector ye must be recomputed.
    isetye=0
    #  if ( msglvl >= 15  &&  msglvl < 30 ) write(nout,1501)
    #  if ( msglvl == 10 ) write(nout,1503)
    # Start according to mode
    ( mode == 2 || mode == 5 || mode == 6 || mode == 7 ) && (@goto step500)
    ( mode == 3 ) && (@goto step100)
    ( mode == 4 ) && (@goto step150)
    ( mode == 8 ) && (@goto step530)
    # Start solving a new problem.
    # Choose as the working set the best column
    # l = argmin{ 0.5*|P(:,j)|^2 + u*a(j)| j = 1:m}
    w=0.5*g[1]+u*a[1]
    l=1
    kg=0
    for j in 1:m
        kg=kg+j
        s=0.5*g[kg]+u*a[j]
        ( w <= s ) && continue
        w=s
        l=j
    end
    # Initialize the working set J = {l}.
    js[1]=l
    # k is the cardinality of the working set.
    k=1
    # yhat is the k-vector containing positive components of x.
    yhat[1]=1
    # lg is the location of (P(:,l) transpose)*P(:,l) in g(*).
    lg=Int64(l*(l+1)/2)
    # Initially v = -[|P(:,l)|^2 + u*a(l)]
    v=-(g[lg]+u*a[l])
    # Initially (R transpose)*R = |P(:,l)|^2 + 1.
    r[1]=sqrt(1+g[lg])
    # lenrk is the number of elements in the upper triangle of R.
    lenrk=1
    # idelet is the number of (internal) column deletions.
    idelet=0
    # Set the initial k-vectors ya and ye.
    ya[1]=-a[l]/r[1]
    ye[1]=1/r[1]
    # #  if ( msglvl >= 200 ) write(nout,1001) ya(1), ye(1)
    #  1001 format(" ya(1)=",1pd22.15," ye(1)=",d22.15)
    jadd=l

    @label step100
    # NOTE: Step 1: stopping criteria.
    #       Compute x, errx and ahat:
    x[1:m] .= 0
    errx=Tf(0)
    ahat=Tf(0)
    for j in 1:k
        ja=js[j]
        x[ja]=yhat[j]
        errx=errx+yhat[j]
        ahat=ahat+a[ja]*yhat[j]
    end
    errx=1-errx
    # Compute vhat, vtilde, l, vgxal, w, violat and errkt:
    vhat=0
    vgxal=0
    errkt=0
    for i in 1:m
        # Find s = sum{G(i,j)*x(j)| j=1:m}.
        s=0
        for j in 1:k
            ja=js[j]
            ig=min(i,ja)
            jg=max(i,ja)
            kg=Int64((jg-1)*jg/2+ig)
            s=s+g[kg]*yhat[j]
        end
        vhat=vhat+x[i]*s
        s=s+u*a[i]
        ( i == 1 ) && (vtilde=-s)
        vtilde=max(vtilde,-s)
        s=s+v
        ( x[i] > 0 ) && (errkt=max(errkt,abs(s)))
        ( s >= vgxal  &&  i > 1 ) && continue
        vgxal=s
        l=i
    end
    #     Here vhat = (x transpose)*G*x.
    w=0.5*vhat+u*ahat
    vhat=-(vhat+u*ahat)
    #     Jump here for an entry with mode = 4.
    @label step150
    #     lg is the location of (P(:,l) transpose)*P(:,l) in g(*).
    lg=Int64(l*(l+1)/2)
    violat=vgxal+epstop*(1+g[lg])
    errorv=max(abs(v-vhat),abs(v-vtilde),abs(vhat-vtilde))/(1+abs(v))
    #     w is the current objective value, wold is the previous one
    ( iwold == 0 ) && (wold=w)
    iwold=1
    deltaw=wold-w
    wold=w
    ( deltaw <= 0 ) && (incrw+=1)
    # #      if ( iter == 0  &&  msglvl >= 15 || msglvl >= 30)
    # #     &   write(nout,1501)
    #  1501 format("  itn jdel jadd    step  k     rho",9x,"w   ",
    #      &       "deltaw",9x,"v  errorv    vgxal")
    # #      if ( msglvl >= 15 ) write(nout,1502) iter, jdel, jadd, t, k,
    # #     &                                     r(lenrk), w, deltaw, v,
    # #     &                                     errorv, vgxal
    #  1502 format(" ",i4,2i5,1pd8.1,i3,d8.1,d10.2,d9.1,d10.2,d8.1,d9.1)
    # #      if ( msglvl == 10  &&  iter == 0 ) write(nout,1503)
    #  1503 format("  itn   k naug nexc ndel",9x,"w   deltaw",10x,"v   ",
    #      &       "errorv    vgxal")
    # #      if ( msglvl == 10 ) write(nout,1504) iter, k, naug, nexc, ndel,
    # #     &                                     w, deltaw, v, errorv, vgxal
    #  1504 format(" ",2i4,3i5,1pd10.2,d9.1,d11.3,2d9.1)
    # #      if ( msglvl >= 30 ) write(nout,1505) (js(j),j=1,k)
    #  1505 format(" js=",18i4)
    # #      if ( msglvl >= 100 ) write(nout,1506) vgxal, violat, vhat
    #  1506 format(" vgxal=",1pd22.15," violat=",d22.15/"  vhat=",d22.15)
    inform=-1
    #     Test the basic stopping criterion
    ( violat >= 0 ) && (inform=0)
    #     Test if the Kuhn-Tucker solution is inaccurate
    for j in 1:k
        if ( l == js[j]  &&  inform < 0 )
            inform=1
            #           Try to refactorize R to reduce errors.
            ( idelet > idelmx ) && (@goto step700)
        end
    end
    #     Test the iteration limit
    ( iter >= itermx  &&  inform < 0 ) && (inform=4)
    #     Test if too many objective increases occured
    ( incrw == nincrw  &&  inform < 0 ) && (inform=2)
    ( incrwm == -1 ) && (wmin=w)
    ( w == wmin ) && (incrwm=incrwm+1)
    if ( w < wmin )
        wmin=w
        incrwm=0
    end
    ( incrwm == nincrw  &&  inform < 0 ) && (inform=2)
    ( inform >= 0 ) && (@goto step900)
    #     NOTE: Step 2: affine independence test
    #           Step 2(i): compute candidate column of R of the form
    #     (     y     )
    #     (sqrt(rhosq))
    #     Find ytilde = e + (Phat transpose)*P(:,l), where
    #     Phat(:,j) = P(:,js(j)), j = 1:k.
    for j in 1:k
        ja=js[j]
        ig=min(l,ja)
        jg=max(l,ja)
        kg=Int64((jg-1)*jg/2+ig)
        #        x stores (Phat transpose)*P(:,l) for Steps 2 and 4.
        x[j]=g[kg]
        ytilde[j]=1+g[kg]
    end
    #     Premultiply ytilde by the inverse of R transpose.
    q1edf2_1(k, lenrk, r, ytilde)
    #     Set y = ytilde, so that
    #     (R transpose)*y = e + (Phat transpose)*P(:,l), and find
    #     rhosq = (y transpose)*y.
    rhosq=0
    for j in 1:k
        y[j]=ytilde[j]
        rhosq=rhosq+ytilde[j]^2
    end
    #     Set rhosq = 1 + G(l,l) - (y transpose)*y.
    rhosq=(1+g[lg])-rhosq
    # #      if ( msglvl >= 120 ) write(nout,2201) rhosq, l, l, g(lg)
    #  2201 format(" At Step 2 rhosq=",1pd22.15,"  g(",i3,",",i3,")=",       d22.15)
    # NOTE Step 2(ii): Would augmented R be sufficiently positive definite?
    if ( rhosq <= epscon*(1+g[lg]) || k == kmax )
        #        Premultiply ytilde by the inverse of R to find the
        #        coefficients of the LS approximation to the new column l.
        q1edf2_0(k, lenrk, r, ytilde)
        #        Compute the least squares errors:
        #        delta = sum{ ytilde(i)| i = 1:k} - 1,
        #        deltap = |Phat*ytilde - P(:,l)|^2,
        #        deltaw = |Phat*ytilde - P(:,l) - P(:,l)*delta|^2,
        #        ybarl = sum{ ytilde(i)| i = 1:k}.
        ybarl=sum(@view ytilde[1:k])
        delta=ybarl-1
        deltap=0
        for i in 1:k
            ia=js[i]
            #           Find s = sum{G(js(i),js(j))*ytilde(j)| j = 1:k}.
            s=0
            for j in 1:k
                ja=js[j]
                ig=min(ia,ja)
                jg=max(ia,ja)
                kg=Int64((jg-1)*jg/2+ig)
                s=s+g[kg]*ytilde[j]
            end
            deltap=deltap+ytilde[i]*s
        end
        #        Here deltap = (Phat*ytilde transpose)*Phat*ytilde.
        #        Recall that x stores (Phat transpose)*P(:,l).
        s::Tf = Tf(0)
        for i in 1:k
            s += ytilde[i] * x[i]
        end
        #        Here s = (P(:,l) transpose)*Phat*ytilde.
        deltaw=(deltap+g[lg]*ybarl^2)-2*ybarl*s
        deltap=abs((deltap+g[lg])-2*s)
        s=rhosq-(delta^2+deltap)
        # #         if ( msglvl >= 120 ) write(nout,2601) delta, deltap, s
        #  2601    format(" At Step 2 delta=",1pd22.15,"  deltap=",d22.15/
        #      &          " rhosq-delta^2-deltap=",d22.15)
        # NOTE Step 2(iii): test if the least squares error delta indicates
        # affine independence of column l from the working set
        if ( delta >= -eps1 || k == kmax )
            #           Calculate stepsize t and position jr of a column in the
            #           working set that might be exchanged with column l
            t=100/(1-eps1)
            jr=0
            for j in 1:k
                ( ytilde[j] <= 0 || t*ytilde[j] < yhat[j] ) && continue
                t=yhat[j]/ytilde[j]
                jr=j
            end
            # #            if ( msglvl >= 120 ) write(nout,2701) t, deltaw, jr
            #  2701       format(" At Step 2 t=",1pd22.15," deltaw=",d22.15,"  jr=",i3)
            #           Test if column exchange will decrease the objective value
            ( 0.5*t*deltaw < (eps2-1-delta)*vgxal ) && (@goto step400)
            #               if ( msglvl >= 100  &&  k == kmax  &&  jr != 0 )
            # #     &            write(nout,2702)
            #  2702          format(" ***Must @goto Step 4 with no objective",
            #      &                " decrease: k = kmax")
            #              Test whether column augmentation cannot occur and, if
            #              so, whether column exchange is possible
            ( k == kmax  &&  jr != 0 ) && (@goto step400)
            # #                  if ( msglvl >= 100  &&  k == kmax ) write(nout,2703)
            #  2703             format(" ****Trouble at Step 2: k=kmax, must refactorize")
            #                 Test whether column augmentation is not possible
            if ( k == kmax )
                #                    Try to refactorize R to recover affine
                #                    independence of the working columns.
                ( idelet > idelmx ) && (@goto step700)
                inform=3
                @goto step900
            end
        end
        #        Modify rhosq to include the least squares errors
        rhosq=max(rhosq,delta^2+deltap)
        # #         if ( msglvl >= 120 ) write(nout,2801) rhosq
        #  2801    format(" Step 2: column exchange would not decrease w, rhosq=",1pd22.15)
        #        Refactorize if the new rhosq = 0.
        # #         if ( msglvl >= 100  &&  rhosq == 0 ) write(nout,2802)
        #  2802    format(" rhosq = 0, must refactorize or stop")
        if ( rhosq == 0 )
            #           Try to refactorize R to recover affine
            #           independence of the working columns.
            ( idelet > idelmx ) && (@goto step700)
            inform=3
            @goto step900
        end
    end

    # NOTE Step 3: column augmentation
    naug=naug+1
    istep=3
    # Here y is the new column of R, and the new elements
    # yanew = [-a(l) - (y transpose)*ya]/rho,
    # yenew = [1 - (y transpose)*ye]/rho,
    # where rho is the new diagonal element of R.
    yanew=-a[l]
    yenew=1
    lenrk=lenrk+1
    for j in 1:k
        r[lenrk]=y[j]
        yanew=yanew-y[j]*ya[j]
        yenew=yenew-y[j]*ye[j]
        lenrk=lenrk+1
    end
    r[lenrk]=sqrt(rhosq)
    # Augment the working set.
    k=k+1
    js[k]=l
    yhat[k]=0
    ya[k]=yanew/r[lenrk]
    ye[k]=yenew/r[lenrk]
    # #  if ( msglvl >= 130 ) write(nout,3101) k, l, r(lenrk)
    #  3101 format(" Step 3: column augmentation, k=",i3," l=",i3, " rho=",1pd22.15)
    jadd=l
    t=0
    @goto step500

    # NOTE Step 4: column exchange
    @label step400
    nexc=nexc+1
    for j in 1:k
        yhat[j]=max(yhat[j]-t*ytilde[j],0)
    end
    # #  if ( msglvl >= 140 ) write(nout,4101) jr, k
    #  4101 format(" Step 4: deleting jr=",i3,", k=",i3)
    istep=4
    # Delete the jr-th column from the working set
    # (this step can also be entered from Step 6 with istep = 6)
    @label step415
    jdel=js[jr]
    if ( jr < k )
        #    Delete column jr from R.
        idelet=idelet+1
        kmin1=k-1
        for j in jr:kmin1
            yhat[j]=yhat[j+1]
            js[j]=js[j+1]
        end
        if ( istep == 4 )
            for j in jr:kmin1
                x[j]=x[j+1]
            end
        end
        # Delete column jr from R, using the three-multiplication form
        # of Givens rotations to restore R to upper triangular form.
        jrotat=jr+1
        kr=Int64(jr*(jr+1)/2-1)
        for jrot in jrotat:k
            # Find the rotation in plane (jrot,jrot + 1).
            kr=kr+jrot
            r1=r[kr]
            r2=r[kr+1]
            dmu=max(abs(r1),abs(r2))
            dtau=dmu*sqrt((r1/dmu)^2+(r2/dmu)^2)
            r[kr]=dtau
            dgamma=r1/dtau
            dsigma=r2/dtau
            dnu=dsigma/(1+dgamma)
            # Apply the rotation to ya.
            dalpha=dgamma*ya[jrot-1]+dsigma*ya[jrot]
            ya[jrot]=dnu*(ya[jrot-1]+dalpha)-ya[jrot]
            ya[jrot-1]=dalpha
            # Apply the rotation to ye.
            dalpha=dgamma*ye[jrot-1]+dsigma*ye[jrot]
            ye[jrot]=dnu*(ye[jrot-1]+dalpha)-ye[jrot]
            ye[jrot-1]=dalpha
            if ( jrot < k )
                # Apply the rotation to columns jrot + 1:k.
                ka=kr
                for j in jrot:kmin1
                    ka=ka+j
                    dalpha=dgamma*r[ka]+dsigma*r[ka+1]
                    r[ka+1]=dnu*(r[ka]+dalpha)-r[ka+1]
                    r[ka]=dalpha
                end
            end
        end
        # Delete column jr from R.
        kr=Int64((jr-1)*jr/2+1)
        ka=kr+jr
        for j in jr:kmin1
            for j1 in 1:j
                r[kr]=r[ka]
                kr=kr+1
                ka=ka+1
            end
            ka=ka+1
        end
    end
    lenrk=lenrk-k
    k=k-1
    # #  if ( msglvl >= 140 ) write(nout,4501) jr, k
    #  4501 format(" Column from position",i4," deleted, k=",i3)
    # If abs[a(jdel)] is large, ya should be recomputed.
    ( abs(a[jdel]) > 1.0  &&  jr <= k) && (isetya=1)
    ( istep == 6 ) && (@goto step500)
    #if ( msglvl >= 30) write(nout,1501)
    #      if ( msglvl >= 15) write(nout,1502) iter, jdel, jadd, t, k,
    #     &                                    r(lenrk)
    # Append column l
    # (this step can also be entered from Step 7 with istep=7)
    @label step460
    if ( k == 0 )
        # Empty working set.
        lenrk=0
        idelet=0
    elseif ( istep == 4 )
        # Recall that x stores (Phat transpose)*P(:,l).
        for j=1:k
            ytilde[j]=1+x[j]
        end
    else
        # Find ytilde = e + (Phat transpose)*P(:,l), where
        # Phat(:,j) = P(:,js(j)), j = 1:k.
        for j in 1:k
            ja=js[j]
            ig=min(l,ja)
            jg=max(l,ja)
            kg=Int64((jg-1)*jg/2+ig)
            ytilde[j]=1+g[kg]
        end
    end
    # Premultiply ytilde by the inverse of R transpose.
    if ( k > 0 )
        q1edf2_1(k, lenrk, r, ytilde)
    end
    # Augment R with ytilde, set s1 = (ytilde transpose)*ytilde
    # and set the new components yanew and yenew of ya and ye.
    yanew=-a[l]
    yenew=1
    lenrk=lenrk+1
    s1=0
    for j in 1:k
        s1=s1+ytilde[j]^2
        r[lenrk]=ytilde[j]
        yanew=yanew-ytilde[j]*ya[j]
        yenew=yenew-ytilde[j]*ye[j]
        lenrk=lenrk+1
    end
    # Set the new diagonal element of R.
    r[lenrk]=sqrt(max((1+g[lg])-s1,0))
    # Augment the working set.
    k=k+1
    js[k]=l
    # Augment yhat if this is not refactorization.
    ( istep == 4 ) && (yhat[k]=t*(1+delta))
    # #  if ( msglvl >= 140 ) write(nout,4901) l, k, r(lenrk)
    #  4901 format(" Column",i4," appended, k=",i3," rho=",1pd8.1)
    # Test nonsingularity of R.
    ( r[lenrk] == 0 ) && (@goto step810)
    # Augment ya and ye.
    ya[k]=yanew/r[lenrk]
    ye[k]=yenew/r[lenrk]
    ( istep == 7 ) && (@goto step750)
    jadd=l

    @label step500
    #  NOTE Step 5
    #     Output results of the completed iteration
    #      if ( msglvl >= 30 ) write(nout,1501)
    #      if ( msglvl >= 15 ) write(nout,1502) iter, jdel, jadd, t, k,
    #     &                                     r(lenrk)
    #      if ( msglvl >= 30 ) write(nout,1505) (js(j),j=1,k)
    if ( msglvl >= 50 )
        ka=0
        for j in 1:k
            ka=ka+j
            y[j]=r[ka]
        end
        # #         write(nout,5101)
        #  5101    format(" diagonal elements of the matrix R")
        # #         write(nout,5102) (y(j),j=1,k)
        #  5102    format(" R(i,i)=",1p3d22.15)
    end
    # Start the next iteration.
    iter=iter+1
    if ( isetya == 1 || isetye == 1 )
        # Reset ya if ysetya = 1, and ye if ysetye = 1.
        for j in 1:k
            ja=js[j]
            ( isetya == 1 ) && (ya[j]=-a[ja])
            ( isetye == 1 ) && (ye[j]=1)
        end
        ( isetya == 1 ) && q1edf2_1(k, lenrk, r, ya)
        ( isetye == 1 ) && q1edf2_1(k, lenrk, r, ye)
        isetya=0
        isetye=0
    end
    # #      if ( msglvl >= 200 ) write(nout,5201) (ya(i),i=1,k)
    #  5201 format(" ya=",1p3d22.15)
    # #      if ( msglvl >= 200 ) write(nout,5202) (ye(i),i=1,k)
    #  5202 format(" ye=",1p3d22.15)
    @label step530
    # Set yaye = (ya transpose)*ye and yesqr = (ye transpose)*ye.
    yaye=0
    yesqr=0
    for j=1:k
        yesqr=yesqr+ye[j]^2
        yaye=yaye+ye[j]*ya[j]
    end
    # The case of mode = 8 is special.
    ( mode == 8 ) && (ustore=u)
    ( mode == 8 ) && (u=1)
    # Solve for v the linear equation yesqr*v = 1 - u*yaye.
    # (This v corresponds to 1 - v, where v is the final v.)
    v=(1-u*yaye)/yesqr
    # If mode = 8, replace v by the negative of its u-sensitivity.
    ( mode == 8 ) && (v=-yaye/yesqr)
    # #  if ( msglvl >= 100 ) write(nout,5401) yaye, yesqr, v
    #  5401 format(" yaye=",1pd22.15," yesqr=",d22.15/" v=",d22.15)
    # Set y = u*ya + v*ye for solving the KT system.
    for j in 1:k
        y[j]=u*ya[j]+v*ye[j]
    end
    # Premultiply y by the inverse of R.
    q1edf2_0(k, lenrk, r, y)
    if ( mode == 8 )
        v=-v
        # Retrieve u.
        u=ustore
        # The u-sensitivity calculation is finished.
        @goto step930
    end
    # Set the final v.
    v=1-v
    # Test feasibility of y.
    jr=0
    for j in 1:k
        ( y[j] <= 0 ) && (jr=j)
    end
    jadd=0
    # #  if ( msglvl >= 150 ) write(nout,5601) v, jr
    #  5601 format(" At Step 5 v=",1pd22.15," jr=",i3)
    if ( jr == 0 )
        # The KT solution y is feasible: proceed to test optimality
        for j in 1:k
            yhat[j] = y[j]
        end
        jdel=0
        t=1
        @goto step100
    end

    @label step600
    # NOTE Step 6: column deletion
    # Determine stepsize t and index jr of the column to be deleted
    ndel=ndel+1
    t=1
    for j in 1:k
        s1=yhat[j]-y[j]
        ( s1 <= 0 ) && continue
        s1=yhat[j]/s1
        ( t < s1 ) && continue
        t=s1
        jr=j
    end
    # Replace yhat by t*y + (1 - t)*yhat.
    for j in 1:k
        yhat[j]=max(yhat[j]+t*(y[j]-yhat[j]),0)
    end
    # #   if ( msglvl >= 160 ) write(nout,6201) t, jr
    #  6201 format(" At Step 6 t=",1pd9.2," jr=",i3)
    # Delete the jr-th column from the working set
    istep=6
    @goto step415

    @label step700
    # NOTE Step 7: refactorization
    # Rebuild the working set from scratch.
    nref=nref+1
    istep=7
    krefac=k
    # #   if ( msglvl >= 170 ) write(nout,7001) k
    #  7001 format(" Start of refactorization to k=",i3)
    k=0
    @label step750
    if ( k < krefac )
        l=js[k+1]
        lg=Int64(l*(l+1)/2)
        # Append column l
        @goto step460
    end
    # #   if ( msglvl >= 170 ) write(nout,7601) k
    #  7601 format(" Refactorized to k=",i3)
    # Reset ya and ye (although they have just been computed).
    isetya=1
    isetye=1
    jdel=-1000
    jadd=1000
    t=0
    @goto step500

    # Trouble shooting
    @label step810
    # #  if ( msglvl >= 100 ) write(nout,8101)
    #  8101 format(" ****Trouble: working columns affinely dependent")
    # Try to refactorize R to recover affine independence of the working columns
    # #  if ( msglvl >= 2  &&  idelet > 0 ) write(nout,8102) idelet
    #  8102 format(" ***Recovering affine independence after ",i5, "deletions")
    ( idelet > 0 ) && (@goto step700)
    # #  if ( msglvl >= 100 ) write(nout,8103) k
    #  8103 format(" ***Last column must be dropped, k=",i3)
    # Delete the last element from the working set
    ndel=ndel+1
    ( incrw == nincrw-1 ) && (incrw=incrw-1)
    jdel=-js[k]
    jadd=0
    t=0
    lenrk=lenrk-k
    k=k-1
    s1=0
    for j=1:k
        s1=s1+yhat[j]
    end
    if ( s1 != 0 )
        #        Scale yhat to make it feasible.
        for j in 1:k
            yhat[j]=yhat[j]/s1
        end
    else
        yhat[1]=1
    end
    #     Recompute ya and ye.
    isetya=1
    isetye=1
    @goto step500


    @label step900
    #     Termination
    #     Set the final x and errx.
    x[1:m] .= 0
    errx=0
    for j in 1:k
        ja=js[j]
        errx=errx+yhat[j]
        x[ja]=yhat[j]
    end
    errx=1-errx
    # #      if ( msglvl > 0  &&  msglvl != 2 ) write(nout,9200) inform,
    # #     &                                                      iter
    #  9200 format(/" Exit q1edf: inform=",i2,"  iter=",i3)
    #      if ( msglvl == 5 || msglvl >= 20 ) write(nout,9201)
    #     &   iter, v, w, k, naug, vhat, deltaw, l, nexc, vtilde, vgxal,
    #     &   errx, ndel, errorv, violat, nref
    # 9201 format(" iter=",i4,6x,"v=",1pd17.9,6x,"w=",d17.9,4x,"k=",i4/
    #     &" naug=",i4,3x,"vhat=",d17.9," deltaw=",d9.1,12x,"l=",i4/
    #     &" nexc=",i4," vtilde=",d17.9,"  vgxal=",d9.1,9x,"errx=",d8.1/
    #     &" ndel=",i4," errorv=",d9.1,8x," violat=",d9.1,9x,"nref=",i2)
    # #      if ( msglvl == 1 ) write(nout,9202) (x(i),i=1,m)
    #  9202 format(" x=",1p5e14.7)

    state.ahat = ahat
    state.v = v
    state.vhat = vhat
    state.vtilde = vtilde
    state.w = w
    state.vgxal = vgxal
    state.iter = iter
    state.k = k
    state.l = l
    state.idelet = idelet
    state.inform = inform
    state.naug = naug
    state.nexc = nexc
    state.ndel = ndel
    state.errkt = errkt
    state.errx = errx
    return

    @label step930
    #Exit for mode = 8 (u-sensitivity calculation).
    inform=0
    x[1:m] .= 0
    for j in 1:k
        ja=js[j]
        x[ja]=y[j]
    end
    # #      if ( msglvl > 0 ) write(nout,9501)
    #  9501 format(/" Exit q1edf: inform=0 after sensitivity analysis")
    # #      if ( msglvl == 1 ) write(nout,9502) (x(i),i=1,m)
    #  9502 format(" dx/du=",1p5d14.7)
    # #      if ( msglvl == 5 || msglvl >= 20) write(nout,9503) v
    #  9503 format(" dv/du=",1pd14.7)
    state.ahat = ahat
    state.v = v
    state.vhat = vhat
    state.vtilde = vtilde
    state.w = w
    state.vgxal = vgxal
    state.iter = iter
    state.k = k
    state.l = l
    state.idelet = idelet
    state.inform = inform
    state.naug = naug
    state.nexc = nexc
    state.ndel = ndel
    state.errkt = errkt
    state.errx = errx
    return
end

# function q1edf2(itrans::Int64, m::Int64, lenr::Int64, r::Vector{Tf}, y::Vector{Tf}) where Tf
#     #*****History:
#     #     Written by Krzysztof C. Kiwiel.
#     #     Date last modified: February 17, 1996.
#     #
#     #*****Body of function q1edf2:
#     i = i1 = ia = iold = j = k = ka = 0
#     if ( itrans == 0 )
#         i=m
#         k=lenr
#         y[i]=y[i]/r[k]
#         @label a
#         ( i == 1 ) && return
#         i1=i
#         k=k-i
#         ka=k
#         i=i-1
#         j=i
#         s=y[i]
#         for ia in i1:m
#             ka=ka+j
#             s=s-r[ka]*y[ia]
#             j=j+1
#         end
#         y[i]=s/r[k]
#         @goto a
#     end

#     y[1]=y[1]/r[1]
#     i=1
#     k=1
#     @label b
#     ( i == m ) && return
#     iold=i
#     i=i+1
#     k=k+1
#     s=y[i]
#     for ia in 1:iold
#         s=s-r[k]*y[ia]
#         k=k+1
#     end
#     y[i]=s/r[k]
#     @goto b
# end
function q1edf2_0(m::Int64, lenr::Int64, r::Vector{Tf}, y::Vector{Tf}) where Tf
    i1 = ia = iold = j = ka = 0

    i=m
    k=lenr
    y[i]=y[i]/r[k]
    @label a
    ( i == 1 ) && return
    i1=i
    k=k-i
    ka=k
    i=i-1
    j=i
    s=y[i]
    for ia in i1:m
        ka=ka+j
        s=s-r[ka]*y[ia]
        j=j+1
    end
    y[i]=s/r[k]
    @goto a
end

function q1edf2_1(m::Int64, lenr::Int64, r::Vector{Tf}, y::Vector{Tf}) where Tf
    i1 = ia = iold = j = ka = 0

    y[1]=y[1]/r[1]
    i=1
    k=1
    @label b
    ( i == m ) && return
    iold=i
    i=i+1
    k=k+1
    s=y[i]
    for ia in 1:iold
        s=s-r[k]*y[ia]
        k=k+1
    end
    y[i]=s/r[k]
    @goto b
end
