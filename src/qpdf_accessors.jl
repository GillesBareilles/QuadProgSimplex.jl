function init(maxm, maxk; Tf = Float64)
    leng = Int64(maxm * (maxm + 1) / 2)
    lenr = Int64(maxk * (maxk + 1) / 2)
    return QPDFState{Tf}(
        m = maxm,              # Problem description
        g = zeros(Tf, leng),
        leng = leng,
        u = 1,
        a = zeros(Tf, maxm),
        epstop = 2.22e-12, # Solver params
        epscon = 2.22e-14,
        itermx = 1000,
        idelmx = 1000,
        msglvl = -1,    # TODO: remove, and @info prints
        kmax = maxk,
        ahat = 0,
        v = 0,
        vhat = 0,
        vtilde = 0,
        w = 0,
        vgxal = 0,
        iter = -1,
        k = -1,         # Solver variables
        js = zeros(Int64, maxk),
        l = 0,
        x = zeros(Tf, maxk),
        yhat = zeros(Tf, maxk),
        ya = zeros(Tf, maxk),
        ye = zeros(Tf, maxk),
        y = zeros(Tf, maxk),
        ytilde = zeros(Tf, maxk),
        r = zeros(Tf, lenr),
        lenr = lenr,
        idelet = 0,     # NOTE: return values
        inform = -1,
        naug = 0,
        nexc = 0,
        ndel = 0,
        errkt = 0,
        errx = 0
    )
end

function set_matrix!(qpstate::QPDFState{Tf}, P::Matrix{Tf}) where Tf
    m = size(P, 1)
    k = 1
    for i in 1:m, j in 1:i
        qpstate.g[k] = P[i, j]
        k += 1
    end
    qpstate.leng = Int64(m * (m+1) / 2)
    return nothing
end
function set_vector!(qpstate::QPDFState{Tf}, a::Vector{Tf}) where Tf
    for i in axes(a, 1)
        qpstate.a[i] = a[i]
    end
end

function get_minimizer(qpstate)
    return qpstate.x[1:qpstate.m]
end
