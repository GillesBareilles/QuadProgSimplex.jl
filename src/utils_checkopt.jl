function checkoptimality(P, a, x; print = false)
    J = x .> eps(Float64)
    return checkoptimality(P, a, x, J; print)
end

raw"""
    $TYPEDSIGNATURES

Check optimality of point `x` with nonnull coordinates `Jac` by checking tolerences
 on the KKT system (2.1) of Kiwiel's paper.

The multiplier `v` is computed so as to solve the second line of the system. We
thus check that $x \ge 0$, $\sum x_i = 1$ and that, for each nonnull coordinate
$x_j$, $v + P_j^{\top} P x + a_j = 0$.
"""
function checkoptimality(P, a, x, Jact; print=false)
    res_posx = max(-minimum(x[Jact]), 0)
    res_sumone = abs(sum(x[Jact]) - 1)

    j = findfirst(Jact)
    v = P[:, j]' * P * x + a[j]
    res_sumonemult = norm([ P[:, j]' * P * x + a[j] - v for j in findall(Jact) ])
    res_nullcoords = norm([x[j] for j in findall( @. !(Jact))])

    ineqmultminval = minimum(ones(size(x)) * v + P'*P*x + a)
    res_ineqmultsign = -min(ineqmultminval, 0)

    if print
        γ = 1e-5
        gradstep = x - γ * ∇F(x, P, a)
        pg = similar(x)
        project_simplex!(pg, gradstep)

        println("Obj val.                : ", 0.5*norm(P * x)^2 + dot(a, x))
        println("x ≥ 0                   : ", res_posx)
        println("∑ x - 1                 : ", res_sumone)
        println("|v + Pⱼ' P x + aⱼ|, j∈J : ", res_sumonemult)
        println("|xⱼ|, j∉J               : ", res_nullcoords)
        println("min(ev + P'Px + a) (≥ 0 ?)   : ", res_ineqmultsign)
        println("|x - proxgrad(x)|       : ", norm(x - pg))
    end

    return max(res_posx, res_sumone, res_sumonemult, res_nullcoords, -min(res_ineqmultsign, 0))
end


function ∇F(x, P, a)
    return P' * P * x + a
end

function project_simplex!(res::Vector{Tf}, α::Vector{Tf}) where Tf
    N = length(α)

    # 1. Sorting
    u = sort(α, rev=true)

    # 2.
    k = 1
    sum_u_1k = u[1]
    while (k < N) && (sum_u_1k + u[k+1] - 1) / (k+1) < u[k+1]
        k += 1
        sum_u_1k += u[k]
    end

    # 3.
    τ = (sum_u_1k - 1) / k

    res .= @. max(α - τ, Tf(0))
    return (res .!= Tf(0))
end
