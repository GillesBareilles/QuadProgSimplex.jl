module QuadProgSimplex


using Printf
using DocStringExtensions
using LinearAlgebra
using Random
using Parameters


const QPS = QuadProgSimplex
export QPS

@enum IterationStatus begin
    iteration_completed
    iteration_failed
    problem_solved
end

include("nearestpointpolytope.jl")

export NearestPointPolytope, NearestPointPolytopeState
export nearest_point_polytope


@with_kw mutable struct QPDFState{Tf}
    m::Int64      # Problem description
    g::Vector{Tf}
    leng::Int64
    u::Tf
    a::Vector{Tf}
    epstop::Tf    # Solver params
    epscon::Tf
    itermx::Int64
    idelmx::Int64
    msglvl::Int64 # TODO: remove and @info prints
    mode::Int64 = 0
    kmax::Int64
    ahat::Tf
    v::Tf
    vhat::Tf
    vtilde::Tf
    w::Tf
    vgxal::Tf
    iter::Int64
    k::Int64      # Solver variables
    js::Vector{Int64}
    l::Int64
    x::Vector{Tf}
    yhat::Vector{Tf}
    ya::Vector{Tf}
    ye::Vector{Tf}
    y::Vector{Tf}
    ytilde::Vector{Tf}
    r::Vector{Tf}
    lenr::Int64
    idelet::Int64 # return values
    inform::Int64
    naug::Int64
    nexc::Int64
    ndel::Int64
    errkt::Tf
    errx::Tf
end

include("qpdf_accessors.jl")
include("qpdf_q1edf1.jl")

raw"""
    $TYPEDSIGNATURES

Solve the problem $min_{x \in \Delta} 0.5 \|Px\|^2 + \langle a, x\rangle$ where $x$
is constrained to the simplex set $\Delta$ (non-negative coordinates that sum
to one).

Reference:
- Kiwiel (1986) A Method for Solving Certain Quadratic Programming Problems
  Arising in Nonsmooth Optimization, IMA Journal of Numerical Analysis.
"""
function qpsimplex(P::Matrix{Tf}, a::Vector{Tf}) where Tf
    @assert size(P, 2) == size(a, 1)
    mat = P' * P
    m = size(a, 1)

    qpstate = init(m, m; Tf)
    set_matrix!(qpstate, P' * P)
    set_vector!(qpstate, a)

    q1edf1!(qpstate)

    @assert qpstate.inform == 0
    return get_minimizer(qpstate)
end

export qpsimplex
export init, set_matrix!, set_vector!, q1edf1!, get_minimizer
export QPDFState

include("utils_checkopt.jl")
# include("utils.jl")
# include("qpsimplex.jl")
# export QPSimplex, QPSimplexState
# export qpsimplex, qpsimplex!

include("QPsimplexinstances.jl")

end
