# QuadProgSimplex.jl

<!-- [![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://GillesBareilles.github.io/QuadProgSimplex.jl/stable) -->
<!-- [![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://GillesBareilles.github.io/QuadProgSimplex.jl/dev) -->
<!-- [![Build Status](https://github.com/GillesBareilles/QuadProgSimplex.jl/actions/workflows/CI.yml/badge.svg?branch=main)](https://github.com/GillesBareilles/QuadProgSimplex.jl/actions/workflows/CI.yml?query=branch%3Amain) -->
<!-- [![Coverage](https://codecov.io/gh/GillesBareilles/QuadProgSimplex.jl/branch/main/graph/badge.svg)](https://codecov.io/gh/GillesBareilles/QuadProgSimplex.jl) -->
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)

Minimize quadratic over the simplex, fast and accurate.

*Algorithms:*
- Minimize a Quadratic Program on the simplex set
  > *A Method for Solving Certain Quadratic Programming Problems Arising in Nonsmooth Optimization*, Krzysztof C. Kiwiel, IMA Journal of Numerical Analysis, 1986
  A julia port of K. Kiwiel's fortran implementation. Very fast, suited to repeated calls.
- Projection of a point on a polytope
  > *Finding the Nearest Point in A Polytope*, Philip Wolfe, Mathematical Programming, 1976

## Usage

Find the minimal norm element of the convex hull of three 2d vectors, parametrized as a convex combination. 
``` julia
julia> using QuadProgSimplex
julia> P = Float64[
           0 3 -2
           2 0 1
       ]
2×3 Matrix{Float64}:
 0.0  3.0  -2.0
 2.0  0.0   1.0

julia> a = zeros(3);

julia> w = qpsimplex(P, a)
3-element Vector{Float64}:
 0.0
 0.42307692307692313
 0.576923076923077

julia> nearest_point_polytope(P)
3-element Vector{Float64}:
 0.0
 0.4230769230769231
 0.5769230769230769
```

